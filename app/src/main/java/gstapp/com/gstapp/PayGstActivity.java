package gstapp.com.gstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import gstapp.com.login_module.LoginActivity;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 4/5/2018.
 */

public class PayGstActivity extends AppCompatActivity {

    ApiInterface apiInterface;
    TextView webView, user_name_pay;
    ImageView back;
    WebView web_View;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_gst);


        String url = "https://payment.gst.gov.in/payment";

        apiInterface = CommonUtils.InitilizeInterface();
        webView = (TextView) findViewById(R.id.textHtml);
        user_name_pay = (TextView) findViewById(R.id.user_name_pay);
        linearLayout = (LinearLayout) findViewById(R.id.log_out);
        web_View = findViewById(R.id.web_View);
        web_View.setWebViewClient(new MyBrowser());

        web_View.getSettings().setLoadsImagesAutomatically(true);
        web_View.getSettings().setJavaScriptEnabled(true);
        web_View.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        web_View.loadUrl(url);

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(PayGstActivity.this);
        user_name_pay.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());

        back = findViewById(R.id.backEdit);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(PayGstActivity.this, LoginActivity.class));
                CommonUtils.makeUserLogout(PayGstActivity.this);
                finish();
            }
        });


//        getAboutContent();

    }

    private void getAboutContent() {

        Call<ResponseBody> bodyCall = apiInterface.getAboutContent();
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (status.equals("200")) {
                            String data = jsonObject.getString("data");
                            webView.setText(Html.fromHtml(data));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error occured,please try again", webView,"300");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
