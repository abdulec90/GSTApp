package gstapp.com.gstapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import ModelClasses.FileBean;
import gstapp.com.Adapters.FileAdapter;

public class PdfActivity extends AppCompatActivity {


    ListView listView;
    String path;
    ArrayList<FileBean> list;
    FileAdapter adapter;
    ArrayList<String> filePaths;
    Button doneButton;

    ImageView backClick;

    void initViews() {
        //views initialization
        listView = (ListView) findViewById(R.id.listView);
        doneButton = (Button) findViewById(R.id.doneButton);
        backClick = (ImageView) findViewById(R.id.backClick);
        list = new ArrayList<>();

        filePaths = new ArrayList<>();

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (filePaths != null) {
                    Intent data = new Intent();

//---set the data to pass back---
                    data.putStringArrayListExtra("linkedList", filePaths);
                    setResult(RESULT_OK, data);
//---close the activity---
                    finish();
                }


            }
        });

        backClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        //get the absolute path of phone storage
        path = Environment.getExternalStorageDirectory().getAbsolutePath();

        //calling the initList that will initialize the list to be given to Adapter for binding data
        initList(path);

        /*for (int i = 0; i < list.size(); i++) {
            filePaths.add(list.get(i).getFilePath());
        }*/

        adapter = new FileAdapter(this, R.layout.pdf_list_item, list, PdfActivity.this);

        //set the adapter on listView
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        //when user chooses a particular pdf file from list,
        //start another activity that will show the pdf
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (filePaths.size() <= 50) {
                    if (!filePaths.contains(list.get(position).getFilePath())) {
                        filePaths.add(list.get(position).getFilePath());
                        list.get(position).setClicked(true);

                        AppCompatCheckedTextView checkBox = (AppCompatCheckedTextView) view;
                        //Toast.makeText(PdfActivity.this, "Added " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        filePaths.remove(list.get(position).getFilePath());
                        list.get(position).setClicked(false);
                        // Toast.makeText(PdfActivity.this, "Removed " + position, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Toast.makeText(PdfActivity.this, "Max 5 Items can be selected ", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    private void initList(String path) {


        File file = new File(path);
        File[] fileArr = file.listFiles();
        String fileName;
        for (File file1 : fileArr) {
            if (file1.isDirectory()) {
                initList(file1.getAbsolutePath());
            } else {
                fileName = file1.getName();
                //choose only the pdf files
                if (fileName.endsWith(".pdf")) {
                    list.add(new FileBean(fileName, file1.getAbsolutePath()));
                }
            }

        }
    }

    //ndling permissions for Android Marshmallow and above
    void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            //if permission granted, initialize the views
            initViews();
        } else {
            //show the dialog requesting to grant permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        setTitle("Pdf Reader");
        //Check if permission is granted(for Marshmallow and higher versions)
        if (Build.VERSION.SDK_INT >= 23)
            checkPermission();
        else
            initViews();
    }
    //Check if permission is granted(for Marshmallow and higher versions)
       /* if (Build.VERSION.SDK_INT >= 23)
            checkPermission();
        else
            initViews();
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initViews();
                } else {
                    //permission is denied (this is the first time, when "never ask again" is not checked)
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        finish();
                    }
                    //permission is denied (and never ask again is  checked)
                    else {
                        //shows the dialog describing the importance of permission, so that user should grant
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("You have forcefully denied Read storage permission.\n\nThis is necessary for the working of app." + "\n\n" + "Click on 'Grant' to grant permission")
                                //This will open app information where user can manually grant requested permission
                                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                })
                                //close the app
                                .setNegativeButton("Don't", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });
                        builder.setCancelable(false);
                        builder.create().show();
                    }
                }
        }
    }

    public void addCurrentPdfToList(int position, CheckBox checkBox) {


        if (!filePaths.contains(list.get(position).getFilePath())) {


            if (filePaths.size() < 5) {
                filePaths.add(list.get(position).getFilePath());
            } else {
                checkBox.setChecked(false);
                Toast.makeText(PdfActivity.this, "Max 5 Items can be selected ", Toast.LENGTH_SHORT).show();

            }

            // Toast.makeText(PdfActivity.this, "Added " + position, Toast.LENGTH_SHORT).show();
        } else {
            filePaths.remove(list.get(position).getFilePath());

            //Toast.makeText(PdfActivity.this, "Removed " + position, Toast.LENGTH_SHORT).show();
        }


    }
}





