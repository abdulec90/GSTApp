package gstapp.com.gstapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ModelClasses.UserProfile;
import UtilClasses.CommonUtils;
import UtilClasses.ImageInterface;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.Adapters.StateCodeAdapter;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class EditProfileActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @BindView(R.id.com_name_edit)
    EditText com_name_edit;

    /*@BindView(R.id.sate_spinner)
    AppCompatSpinner stateSpinner;*/

    @BindView(R.id.acc_type_spinner)
    AppCompatSpinner acc_type_spinner;

    @BindView(R.id.com_phone_edit)
    EditText com_phone_edit;

    @BindView(R.id.com_email_edit)
    EditText com_email_edit;

    @BindView(R.id.com_GSTN_edit)
    EditText com_GSTN_edit;

    @BindView(R.id.com_CIN_edit)
    EditText com_CIN_edit;

    @BindView(R.id.com_PAN_edit)
    EditText com_PAN_edit;

    @BindView(R.id.com_add_edit)
    EditText com_add_edit;

    @BindView(R.id.com_state_code_edit)
    EditText com_state_code_edit;

    @BindView(R.id.com_bank_name_edit)
    EditText com_bank_name_edit;

    @BindView(R.id.com_acc_name_edit)
    EditText com_acc_name_edit;

    @BindView(R.id.com_branch_edit)
    EditText com_branch_edit;

    @BindView(R.id.com_acc_no_edit)
    EditText com_acc_no_edit;

    @BindView(R.id.com_IFSC_edit)
    EditText com_IFSC_edit;

    @BindView(R.id.com_bill_no_edit)
    EditText com_bill_no_edit;

    @BindView(R.id.update_button)
    Button update_button;

    @BindView(R.id.com_logo_edit)
    ImageView com_logo_edit;

    @BindView(R.id.backEdit)
    ImageView backEdit;

    @BindView(R.id.user_name)
    TextView user_name;

    @BindView(R.id.stateNameTxt)
    TextView stateNameTxt;

    ApiInterface apiInterface;

    ImageInterface imageInterface;

    private final int PICK_FROM_GALLERY = 200;

    LinkedList<String> ahaha=new LinkedList<>();

    MultipartBody.Part imagePart;
    private Uri uri;
    HashMap<String, String> stateCodes;
    private String stateName, accountType;
    private StateCodeAdapter clad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setMessage("Updating Details");
        progressDialog.setCancelable(false);

        List<UserProfile> userProfile = getIntent().getParcelableArrayListExtra("compnyData");


        stateCodes = new HashMap<>();


        ArrayList<String> accTypeList = new ArrayList<>();
        accTypeList.add("Saving");
        accTypeList.add("Current");


        String cName = userProfile.get(0).getCompany_name();
        prepareHashMap(stateCodes);
        if (userProfile.get(0).getCompany_logo() != null && !userProfile.get(0).getCompany_logo().equals("")) {
            Picasso.get().load(userProfile.get(0).getCompany_logo()).into(com_logo_edit);
        } else {
            com_logo_edit.setImageResource(R.drawable.ic_playstore_icon);
        }

        com_name_edit.setText(userProfile.get(0).getCompany_name());
        com_add_edit.setText(userProfile.get(0).getAddress());
        com_phone_edit.setText(userProfile.get(0).getMobile());
        com_email_edit.setText(userProfile.get(0).getEmail());
        com_GSTN_edit.setText(userProfile.get(0).getGstn());
        com_CIN_edit.setText(userProfile.get(0).getCin());
        com_PAN_edit.setText(userProfile.get(0).getPan());
        //stateSpinner.setSelected(stateList.get());
        stateName = userProfile.get(0).getState();
        com_state_code_edit.setText(userProfile.get(0).getState_code());
        com_bank_name_edit.setText(userProfile.get(0).getBank_name());
        com_acc_name_edit.setText(userProfile.get(0).getFirst_name());
        com_branch_edit.setText(userProfile.get(0).getBank_branch());
        com_acc_no_edit.setText(userProfile.get(0).getAc_no());
        com_IFSC_edit.setText(userProfile.get(0).getIfsc_code());
        com_bill_no_edit.setText(userProfile.get(0).getBill_no_series());


        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(EditProfileActivity.this);
        user_name.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        com_logo_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        update_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateAndUpdate();
            }
        });

        stateNameTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openStateDialog();
            }
        });

        /*ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.custom_spinner_layout, stateList);
        stateSpinner.setAdapter(arrayAdapter);*/

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.custom_spinner_layout, accTypeList);
        acc_type_spinner.setAdapter(typeAdapter);

        /*stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = stateSpinner.getSelectedItem().toString();
                stateName = content;
                com_state_code_edit.setText(stateCodes.get(content));
                //  String newStateCode = stateCodes.get(content);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        acc_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String content = acc_type_spinner.getSelectedItem().toString();
                accountType = content;
                //com_state_code_edit.setText(stateCodes.get(content));
                //  String newStateCode = stateCodes.get(content);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void openStateDialog() {

        final ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);

        ListView marital_lv;

        final Dialog dialog = new Dialog(EditProfileActivity.this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_occupation);
        // dialog.getWindow().getAttributes().windowAnimations = R.style.animationdialog;
        ImageView leftarrow = (ImageView) dialog.findViewById(R.id.leftarrow);
        final EditText editMobileNo = (EditText) dialog.findViewById(R.id.editMobileNo);
        marital_lv = (ListView) dialog.findViewById(R.id.listview);
        //TextView saveImage = (TextView) dialog.findViewById(R.id.saveImage);
        clad = new StateCodeAdapter(EditProfileActivity.this, stateList);
        marital_lv.setAdapter(clad);

        editMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                clad.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*if ((muserDetails.getMarital_status() != null)) {
            clad.setSelection(muserDetails.getMarital_status());
        }*/

        marital_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  visibleGreenTick();
               /* clad.selectedPosition = position;
                maritalValues = stateList[position];
                editor.putString("Marital_status", maritalValues);
                editor.commit();
                clad.notifyDataSetChanged();*/
                stateName = stateList.get(position);
                stateNameTxt.setText(stateName);
                com_state_code_edit.setText(stateCodes.get(stateName));

                InputMethodManager ipmm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                ipmm.hideSoftInputFromWindow(editMobileNo.getWindowToken(), 0);

                dialog.dismiss();

            }
        });


        /*saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // maritalStatus.setText(maritalValues);
                dialog.dismiss();
            }
        });*/

        dialog.show();

    }

    private void validateAndUpdate() {

        if (com_acc_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Name", com_acc_name_edit, "300");
        } else if (com_add_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Address", com_acc_name_edit, "300");
        } else if (com_phone_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Phone", com_acc_name_edit, "300");
        } else if (com_GSTN_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter GSTN No", com_acc_name_edit, "300");
        } else if (com_CIN_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter CIN", com_acc_name_edit, "300");
        } else if (com_PAN_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter PAN", com_acc_name_edit, "300");
        } else if (com_bank_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Bank Name", com_acc_name_edit, "300");
        } else if (com_acc_name_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Account Holder Name", com_acc_name_edit, "300");
        } else if (com_branch_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Branch Name", com_acc_name_edit, "300");
        } else if (com_acc_no_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Account No", com_acc_name_edit, "300");
        } else if (com_IFSC_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter IFSC code", com_acc_name_edit, "300");
        } else if (com_bill_no_edit.length() == 0) {
            CommonUtils.snackBar("Please Enter Bill Serial No", com_acc_name_edit, "300");
        } else {
            progressDialog.show();
            updateUserData();
        }
    }

    private void prepareHashMap(HashMap<String, String> stateCodes) {

        stateCodes.put("Andaman and Nicobar Islands", "35");
        stateCodes.put("Andhra Pradesh", "37");
        stateCodes.put("Arunachal Pradesh", "12");
        stateCodes.put("Assam", "18");
        stateCodes.put("Bihar", "10");
        stateCodes.put("Chandigarh", "04");
        stateCodes.put("Chhattisgarh", "22");
        stateCodes.put("Dadra and Nagar Haveli", "26");
        stateCodes.put("Daman and Diu", "25");
        stateCodes.put("Delhi", "07");
        stateCodes.put("Goa", "30");
        stateCodes.put("Gujarat", "24");
        stateCodes.put("Haryana", "06");
        stateCodes.put("Himachal Pradesh", "02");
        stateCodes.put("Jammu & Kashmir", "01");
        stateCodes.put("Jharkhand", "20");
        stateCodes.put("Karnataka", "29");
        stateCodes.put("Kerala", "32");
        stateCodes.put("Lakshadweep Islands", "31");
        stateCodes.put("Madhya Pradesh", "23");
        stateCodes.put("Maharashtra", "27");
        stateCodes.put("Manipur", "14");
        stateCodes.put("Meghalaya", "17");
        stateCodes.put("Mizoramh", "15");
        stateCodes.put("Nagaland", "13");
        stateCodes.put("Odisha", "21");
        stateCodes.put("Pondicherry", "34");
        stateCodes.put("Punjab", "03");
        stateCodes.put("Rajasthan", "08");
        stateCodes.put("Sikkim", "11");
        stateCodes.put("Tamil Nadu", "33");
        stateCodes.put("Telangana", "36");
        stateCodes.put("Tripura", "16");
        stateCodes.put("Uttar Pradesh", "09");
        stateCodes.put("Uttarakhand", "05");
        stateCodes.put("West Bengal", "19");

    }

    private void prepareStateList(ArrayList<String> stateList) {

        stateList.add("Andaman and Nicobar Islands");
        stateList.add("Andhra Pradesh");
        stateList.add("Arunachal Pradesh");
        stateList.add("Assam");
        stateList.add("Bihar");
        stateList.add("Chandigarh");
        stateList.add("Chhattisgarh");
        stateList.add("Dadra and Nagar Haveli");
        stateList.add("Daman and Diu");
        stateList.add("Delhi");
        stateList.add("Goa");
        stateList.add("Gujarat");
        stateList.add("Haryana");
        stateList.add("Himachal Pradesh");
        stateList.add("Jammu & Kashmir");
        stateList.add("Jharkhand");
        stateList.add("Karnataka");
        stateList.add("Kerala");
        stateList.add("Lakshadweep Islands");
        stateList.add("Madhya Pradesh");
        stateList.add("Maharashtra");
        stateList.add("Manipur");
        stateList.add("Meghalaya");
        stateList.add("Mizoramh");
        stateList.add("Nagaland");
        stateList.add("Odisha");
        stateList.add("Pondicherry");
        stateList.add("Punjab");
        stateList.add("Rajasthan");
        stateList.add("Sikkim");
        stateList.add("Tamil Nadu");
        stateList.add("Telangana");
        stateList.add("Tripura");
        stateList.add("Uttar Pradesh");
        stateList.add("Uttarakhand");
        stateList.add("West Bengal");
    }


    private void openGallery() {

        getUserPermission();

    }

    private void getUserPermission() {

        try {
            if (ActivityCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
            } else {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUserData() {


        String userId = CommonUtils.getUserId(EditProfileActivity.this);
        HashMap<String, String> hashMap = new HashMap<>();

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), com_phone_edit.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), com_add_edit.getText().toString());
        RequestBody com_name = RequestBody.create(MediaType.parse("text/plain"), com_name_edit.getText().toString());
        RequestBody gstn = RequestBody.create(MediaType.parse("text/plain"), com_GSTN_edit.getText().toString());
        RequestBody cin = RequestBody.create(MediaType.parse("text/plain"), com_CIN_edit.getText().toString());
        RequestBody pan = RequestBody.create(MediaType.parse("text/plain"), com_PAN_edit.getText().toString());
        RequestBody state_code = RequestBody.create(MediaType.parse("text/plain"), com_state_code_edit.getText().toString());
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateName);
        RequestBody b_name = RequestBody.create(MediaType.parse("text/plain"), com_bank_name_edit.getText().toString());
        RequestBody b_branch = RequestBody.create(MediaType.parse("text/plain"), com_branch_edit.getText().toString());
        RequestBody bill_no = RequestBody.create(MediaType.parse("text/plain"), com_bill_no_edit.getText().toString());
        RequestBody ac_no = RequestBody.create(MediaType.parse("text/plain"), com_acc_no_edit.getText().toString());
        RequestBody ifsc_code = RequestBody.create(MediaType.parse("text/plain"), com_IFSC_edit.getText().toString());
        RequestBody ac_type = RequestBody.create(MediaType.parse("text/plain"), accountType);

        saveBillNoToLocal(com_bill_no_edit.getText().toString());

        ArrayList<String> mSelectedImages = new ArrayList<>();
        MultipartBody.Part[] body = null;
        if (uri != null) {
            String path = getRealPathFromURIPath(uri, EditProfileActivity.this);
            mSelectedImages.add(path);

            body = prepareFilePartArray("profile_pic", mSelectedImages);
        }


        Call<ResponseBody> responseBodyCall = apiInterface.updateUserData(user_id, mobile, address, com_name, gstn, cin, pan,
                state_code, state, b_name, b_branch, bill_no, ac_no, ifsc_code, ac_type, body);

        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String message = jsonObject.getString("message");
                    CommonUtils.isProfile = true;
                    finish();
                    //Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Log.e("Error", t.toString());
                Toast.makeText(EditProfileActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void saveBillNoToLocal(String s) {

        CommonUtils.saveBillNo(EditProfileActivity.this, s);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            uri = data.getData();
            try {

                String filePath = getRealPathFromURIPath(uri, EditProfileActivity.this);
                File file = new File(filePath);
                Log.d(TAG, "Filename " + file.getName());

                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
                imagePart = fileToUpload;
                RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                com_logo_edit.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private String getRealPathFromURIPath(Uri contentURI, FragmentActivity activity) {

        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }


    @NonNull
    private MultipartBody.Part[] prepareFilePartArray(String partName, ArrayList<String> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            String image = fileUri.get(index);
            File file = new File(String.valueOf(image));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(image);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to /
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }
        // MultipartBody.Part is used to send also the actual file name
        return arrayImage;
    }
}
