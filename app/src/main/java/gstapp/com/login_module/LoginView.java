package gstapp.com.login_module;


import android.content.SharedPreferences;

public interface LoginView {


    void showProgress();

    void hideProgress();

    void setUsernameError();

    void setPasswordError();

    void navigateToDashBoard();

    SharedPreferences getSharedPreferences();

    void roleError(String message);
}
