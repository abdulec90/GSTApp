package gstapp.com.login_module;

/**
 * Created by Abdul on 4/6/2018.
 */

public interface LoginPresenter {

    void validateCredentials(String username, String password);

    void onDestroy();

}
