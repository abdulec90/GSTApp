package gstapp.com.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ModelClasses.ProfileMainPojo;
import ModelClasses.UserProfile;
import UtilClasses.CommonUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.EditProfileActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyProfile extends Fragment {


    private OnFragmentInteractionListener mListener;
    private ApiInterface apiInterface;
    ProgressDialog progressDialog;

    @BindView(R.id.com_name)
    TextView com_name;

    @BindView(R.id.com_address)
    TextView com_address;

    @BindView(R.id.com_phone1)
    TextView com_phone1;

    @BindView(R.id.com_phone2)
    TextView com_phone2;

    @BindView(R.id.com_email1)
    TextView com_email1;

    @BindView(R.id.com_email2)
    TextView com_email2;

    @BindView(R.id.GSTNTxt)
    TextView GSTNTxt;

    @BindView(R.id.CINtxt)
    TextView CINTxt;

    @BindView(R.id.PANTxt)
    TextView PANTxt;

    @BindView(R.id.stateCode_txt)
    TextView stateCodeTxt;

    @BindView(R.id.state_txt)
    TextView state_txt;

    @BindView(R.id.ac_name_txt)
    TextView ac_name_txt;

    @BindView(R.id.bnkName_txt)
    TextView bnkName_txt;

    @BindView(R.id.bank_branch_txt)
    TextView bank_branch_txt;

    @BindView(R.id.AC_no_txt)
    TextView AC_no_txt;

    @BindView(R.id.IFSC_txt)
    TextView IFSC_txt;

    @BindView(R.id.ac_type_txt)
    TextView ac_type_txt;

    @BindView(R.id.com_logo)
    ImageView com_logo;


    @BindView(R.id.bill_serial_txt)
    TextView bill_serial_txt;

    @BindView(R.id.mainLAyout)
    LinearLayout mainLAyout;

    @BindView(R.id.edit_layout)
    RelativeLayout edit_layout;
    private List<UserProfile> userProfiles;

    public MyProfile() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MyProfile newInstance() {

        MyProfile fragment = new MyProfile();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();


        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        apiInterface = APIClient.getClient().create(ApiInterface.class);
        getUserData();
        /*progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        getUserData();*/


        edit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putParcelableArrayListExtra("compnyData", (ArrayList<? extends Parcelable>) userProfiles);
                getActivity().startActivity(intent);

            }
        });

    }

    private void replaceEditFragment() {


        ((DashBoardActivity) getActivity()).headertitle.setVisibility(View.VISIBLE);
        ((DashBoardActivity) getActivity()).headertitle.setText("Update Details");
        EditProfile homeFragment = EditProfile.newInstance();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("compnyData", (ArrayList<? extends Parcelable>) userProfiles);
        homeFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainLayout, homeFragment).commit();

    }

    private void getUserData() {

        String id = CommonUtils.getUserId(getActivity());
        Call<ProfileMainPojo> responseBodyCall = apiInterface.userProfile(id);
        responseBodyCall.enqueue(new Callback<ProfileMainPojo>() {
            @Override
            public void onResponse(Call<ProfileMainPojo> call, Response<ProfileMainPojo> response) {

                progressDialog.dismiss();
                mainLAyout.setVisibility(View.VISIBLE);
                ProfileMainPojo profileMainPojo = response.body();
                String status = profileMainPojo.getStatus();
                if (status.equals("200")) {
                    userProfiles = profileMainPojo.getUser();

                    String companyLogo = userProfiles.get(0).getCompany_logo();

                    if (companyLogo != null && !companyLogo.equals("")) {
                        Picasso.get().load(companyLogo).into(com_logo);
                    } else {
                        com_logo.setImageResource(R.drawable.ic_playstore_icon);
                    }

                    com_name.setText(userProfiles.get(0).getCompany_name());
                    com_address.setText(userProfiles.get(0).getAddress());
                    com_phone1.setText(userProfiles.get(0).getMobile());
                    com_email1.setText(userProfiles.get(0).getEmail());

                    GSTNTxt.setText(userProfiles.get(0).getGstn());
                    CINTxt.setText(userProfiles.get(0).getCin());
                    PANTxt.setText(userProfiles.get(0).getPan());

                    stateCodeTxt.setText(userProfiles.get(0).getState_code());
                    state_txt.setText(userProfiles.get(0).getState());

                    ac_name_txt.setText(userProfiles.get(0).getFirst_name() + " " + userProfiles.get(0).getMid_name()
                            + " " + userProfiles.get(0).getLast_name());
                    bnkName_txt.setText(userProfiles.get(0).getBank_name());
                    bank_branch_txt.setText(userProfiles.get(0).getBank_branch());
                    AC_no_txt.setText(userProfiles.get(0).getAc_no());
                    IFSC_txt.setText(userProfiles.get(0).getIfsc_code());
                    ac_type_txt.setText(userProfiles.get(0).getAc_type());

                    String bill_no_series = userProfiles.get(0).getBill_no_series();

                    bill_serial_txt.setVisibility(View.VISIBLE);
                    bill_serial_txt.setText("Invoice Series No : " + bill_no_series);

                }
            }

            @Override
            public void onFailure(Call<ProfileMainPojo> call, Throwable t) {

                Log.d("Error", t.toString());
                progressDialog.dismiss();
                CommonUtils.snackBar(t.toString(), edit_layout, "300");

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
