package gstapp.com.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ModelClasses.AllBillMainPojo;
import ModelClasses.AllBillsListPojo;
import UtilClasses.CommonUtils;
import gstapp.com.Adapters.MyAllBillAdapter;
import gstapp.com.GalleryViewActivity;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewAllPurchaseBills extends Fragment {


    RecyclerView allBillRecycler;

    ApiInterface apiInterface;
    private ArrayList<String> categoryList;
    private List<AllBillsListPojo> allBillsListPojos;

    ProgressDialog progressDialog;
    private MyAllBillAdapter myAllBillAdapter;
    private Context context;

    public ViewAllPurchaseBills() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ViewAllPurchaseBills newInstance() {
        ViewAllPurchaseBills fragment = new ViewAllPurchaseBills();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_view_all_purchase_bills, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        context = getActivity();

        allBillRecycler = (RecyclerView) getView().findViewById(R.id.allBillRecycler);
        allBillRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        categoryList = new ArrayList<>();
        myAllBillAdapter = new MyAllBillAdapter(getActivity(), allBillsListPojos, ViewAllPurchaseBills.this, categoryList);
        allBillRecycler.setAdapter(myAllBillAdapter);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");

        getAllBills();
    }

    private void getAllBills() {

        apiInterface = CommonUtils.InitilizeInterface();
        if (context != null) {
            Call<AllBillMainPojo> bodyCall = apiInterface.getAllBillsImages(CommonUtils.getUserId(context));
            bodyCall.enqueue(new Callback<AllBillMainPojo>() {
                @Override
                public void onResponse(Call<AllBillMainPojo> call, Response<AllBillMainPojo> response) {

                    if (response != null) {
                        progressDialog.dismiss();
                        AllBillMainPojo myBillMainPojo = response.body();
                        allBillsListPojos = myBillMainPojo.getResponse();
                        categoryList.clear();
                        for (int i = 0; i < allBillsListPojos.size(); i++) {
                            String catgery = allBillsListPojos.get(i).getCategory();
                            for (int j = 0; j < allBillsListPojos.size(); j++) {
                                String cat2 = allBillsListPojos.get(j).getCategory();
                                if (cat2.equals(catgery) && !categoryList.contains(cat2)) {
                                    categoryList.add(cat2);
                                }
                            }

                            //myAllBillAdapter.notifyDataSetChanged();

                        }

                        MyAllBillAdapter myAllBillAdapter = new MyAllBillAdapter(getActivity(), allBillsListPojos, ViewAllPurchaseBills.this, categoryList);
                        allBillRecycler.setAdapter(myAllBillAdapter);

                    }

                    String image = allBillsListPojos.get(0).getCompany_id();

                }

                @Override
                public void onFailure(Call<AllBillMainPojo> call, Throwable t) {

                    CommonUtils.snackBar(t.toString(), allBillRecycler, "300");
                    // progressDialog.dismiss();
                }
            });
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showGallery(ArrayList<String> imagesList, String title) {

        Intent intent = new Intent(getActivity(), GalleryViewActivity.class);
        intent.putExtra("title", title);
        intent.putStringArrayListExtra("imagesList", imagesList);
        getActivity().startActivity(intent);


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getAllBills();
        }
    }
}
