package gstapp.com.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import UtilClasses.CommonUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Setting extends Fragment {


    EditText currentPass;
    EditText newPass;
    EditText confirmPass;
    Button submitButton;

    ApiInterface apiInterface;

    public static Setting newInstance() {
        Setting fragment = new Setting();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = CommonUtils.InitilizeInterface();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        currentPass = (EditText) view.findViewById(R.id.currentPassEdit);
        newPass = (EditText) view.findViewById(R.id.newPassEdit);
        confirmPass = (EditText) view.findViewById(R.id.confirmPassEdit);
        submitButton = (Button) view.findViewById(R.id.submitpassButton);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldPass = currentPass.getText().toString();
                String newPassword = newPass.getText().toString();
                String confPass = confirmPass.getText().toString();
                if (oldPass.length() != 0 && newPassword.length() != 0 && confPass.length() != 0) {

                    if (newPassword.equals(confPass)) {
                        Call<ResponseBody> bodyCall = apiInterface.changePassword(CommonUtils.getUserId(getActivity()), oldPass, newPassword);

                        bodyCall.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                if (response != null) {
                                    try {
                                        String res = response.body().string();
                                        JSONObject jsonObject = new JSONObject(res);
                                        String code = jsonObject.getString("code");
                                        String msg = jsonObject.getString("message");
                                        if (code.equals("200")) {
                                            CommonUtils.snackBar(msg, currentPass, code);

                                        } else {
                                            CommonUtils.snackBar(msg, currentPass, code);

                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {

                                Log.v("Error", t.toString());
                            }
                        });

                    } else {
                        CommonUtils.snackBar("New Password and confirm password should be same", currentPass, "300");
                    }

                } else {

                }


            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
