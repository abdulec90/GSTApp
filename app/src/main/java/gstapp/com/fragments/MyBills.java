package gstapp.com.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ModelClasses.MyBillDetailPojo;
import ModelClasses.MyBillMainPojo;
import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import gstapp.com.Adapters.MyBillsAdapter;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;


public class MyBills extends Fragment {


    RecyclerView recyclerView;

    ApiInterface apiInterface;
    String userId;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 977;

    private TextView fromDate, toDate, noOfInvoice, turnOver;

    EditText searchEdit;
    private String selectedFromDate;

    DatePickerDialog datePickerDialog;
    MyBillsAdapter myBillsAdapter;

    private ProgressDialog progressDialog;
    private String selectedToDate;
    private View content;
    private LinearLayout view;
    Bitmap bitmap;

    private String totalTaxAmount;
    private Dialog mDialog;


    public static MyBills newInstance() {
        MyBills fragment = new MyBills();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_bills, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = (RecyclerView) getView().findViewById(R.id.myBillsRecycler);
        searchEdit = (EditText) getView().findViewById(R.id.searchEdit);
        userId = CommonUtils.getUserId(getActivity());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        fromDate = (TextView) getView().findViewById(R.id.fromDateTxt);
        toDate = (TextView) getView().findViewById(R.id.toDateTxt);
        noOfInvoice = (TextView) getView().findViewById(R.id.btn_no_of_invice);
        turnOver = (TextView) getView().findViewById(R.id.btn_turn_over);


        searchEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (searchEdit.getRight() - searchEdit.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here

                        InputMethodManager imm = (InputMethodManager) getActivity()
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(((Activity) getActivity()).getWindow()
                                .getCurrentFocus().getWindowToken(), 0);

                        progressDialog.show();
                        getFilteredData();

                        return true;
                    }
                }
                return false;
            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedFromDate == null || selectedFromDate.equals("")) {
                    CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                } else {
                    openDatePickerTo();
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        apiInterface = APIClient.getClient().create(ApiInterface.class);
        getBillList();

        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //String text = searchEdit.getText().toString().toLowerCase(Locale.getDefault());
                // myBillsAdapter.filter(text);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void getFilteredData() {

        String id = CommonUtils.getUserId(getActivity());
        Call<MyBillMainPojo> bodyCall = apiInterface.getFilteredData(id, selectedFromDate, selectedToDate, searchEdit.getText().toString());

        bodyCall.enqueue(new Callback<MyBillMainPojo>() {
            @Override
            public void onResponse(Call<MyBillMainPojo> call, Response<MyBillMainPojo> response) {

                progressDialog.dismiss();
                if (response != null) {
                    MyBillMainPojo myBillMainPojo = response.body();
                    String status = myBillMainPojo.getStatus();
                    String msg = myBillMainPojo.getMessage();
                    String basicRate = myBillMainPojo.getBasicRate();
                    String totalInvoice = myBillMainPojo.getTotalInvoice();

                    List<MyBillDetailPojo> myBillDetailPojo = myBillMainPojo.getBills();
                    if (myBillDetailPojo.size() == 0) {
                        Toast.makeText(getActivity(), "No Bill Found", Toast.LENGTH_SHORT).show();
                        recyclerView.setAdapter(null);
                    } else {

                        noOfInvoice.setText("Total Count : " + myBillMainPojo.getTotalInvoice());
                        turnOver.setText("Turn Over : " + myBillMainPojo.getBasicRate());

                        myBillsAdapter = new MyBillsAdapter(getActivity(), myBillDetailPojo, MyBills.this);
                        recyclerView.setAdapter(myBillsAdapter);
                    }

                }

            }

            @Override
            public void onFailure(Call<MyBillMainPojo> call, Throwable t) {

            }
        });

    }

    private void openDatePickerTo() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                toDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);
                selectedToDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    if (selectedFromDate == null || selectedFromDate.equals("")) {
                        CommonUtils.snackBar("Please Select From Date", searchEdit, "300");
                    } else {
                        Date date1 = sdf.parse(selectedFromDate);
                        Date date2 = sdf.parse(selectedToDate);

                        if (date1.compareTo(date2) > 0) {
                            System.out.println("Date1 is after Date2");
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        } else if (date1.compareTo(date2) < 0) {
                            System.out.println("Date1 is before Date2");
                            //call API.........................
                            progressDialog.show();
                            getFilteredData();

                        } else if (date1.compareTo(date2) == 0) {
                            Toast.makeText(getActivity(), "Please select ToDate Greater then FromDate", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
        // date picker dialog

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                fromDate.setText(dayOfMonth + "/"
                        + (monthOfYear + 1) + "/" + year);

                selectedFromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private void getBillList() {

        String id = CommonUtils.getUserId(getActivity());

        Call<MyBillMainPojo> myBillMainPojoCall = apiInterface.getBillList(id);

        myBillMainPojoCall.enqueue(new Callback<MyBillMainPojo>() {
            @Override
            public void onResponse(Call<MyBillMainPojo> call, Response<MyBillMainPojo> response) {

                if (progressDialog != null)
                    progressDialog.dismiss();
                if (response != null) {
                    MyBillMainPojo myBillMainPojo = response.body();
                    String status = myBillMainPojo.getStatus();
                    String msg = myBillMainPojo.getMessage();

                    noOfInvoice.setText("Total Count : " + myBillMainPojo.getTotalInvoice());
                    turnOver.setText("Turn Over : " + myBillMainPojo.getBasicRate());

                    List<MyBillDetailPojo> myBillDetailPojo = myBillMainPojo.getBills();
                    myBillsAdapter = new MyBillsAdapter(getActivity(), myBillDetailPojo, MyBills.this);
                    recyclerView.setAdapter(myBillsAdapter);
                }
            }

            @Override
            public void onFailure(Call<MyBillMainPojo> call, Throwable t) {

                progressDialog.dismiss();
                CommonUtils.snackBar("Please Check Internet connection", recyclerView, "300");

            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void openDialog(final MyBillDetailPojo myBillDetailPojo, Context context) {

        mDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.getWindow();
        mDialog.setContentView(R.layout.bitmap_list_view);

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.5f;
        lp.horizontalMargin = 20;
        lp.verticalMargin = 20;


        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(getActivity());

        // draw something on the page
        LayoutInflater inflater = (LayoutInflater)
                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = inflater.inflate(R.layout.pdf_layout, null);
        RelativeLayout relativeLayout = (RelativeLayout) content.findViewById(R.id.rotateRelative);
        relativeLayout.setRotation(90);

        TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
        ImageView pdf_logo = (ImageView) content.findViewById(R.id.pdf_logo);
        TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
        TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
        TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

        Picasso.get().load(logeedUserDetails.getCompany_logo()).into(pdf_logo);

        TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
        TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
        TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
        TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
        TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
        TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
        TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);
        TextView igst_infigure = (TextView) content.findViewById(R.id.igst_infigure);
        TextView bottom_com_name = (TextView) content.findViewById(R.id.bottom_com_name);
        //............set Bank Details..............................................................
        TextView ac_name_pdf = (TextView) content.findViewById(R.id.ac_name_pdf);
        TextView bnkName_pdf = (TextView) content.findViewById(R.id.bnkName_pdf);
        TextView bank_branch_pdf = (TextView) content.findViewById(R.id.bank_branch_pdf);
        TextView AC_no_pdf = (TextView) content.findViewById(R.id.AC_no_pdf);
        TextView IFSC_pdf = (TextView) content.findViewById(R.id.IFSC_pdf);
        TextView ac_type_pdf = (TextView) content.findViewById(R.id.ac_type_pdf);

        ac_name_pdf.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());
        bnkName_pdf.setText(logeedUserDetails.getBank_name());
        bank_branch_pdf.setText(logeedUserDetails.getBank_branch());
        AC_no_pdf.setText(logeedUserDetails.getAc_no());
        IFSC_pdf.setText(logeedUserDetails.getIfsc_code());
        ac_type_pdf.setText(logeedUserDetails.getAc_type());

        TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
        TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
        TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
        TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
        TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
        TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
        TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
        TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
        TextView grand_total = (TextView) content.findViewById(R.id.grand_total);

        PdfComName.setText(logeedUserDetails.getCompany_name());
        PdfComAdd.setText(logeedUserDetails.getAddress());

        pdf_gstn.setText("GSTN : " + logeedUserDetails.getGstn());
        pdf_cin.setText("CIN  : " + logeedUserDetails.getCin());
        pdf_pan.setText("PAN : " + logeedUserDetails.getPan());
        pdf_state_code.setText("State Code : " + logeedUserDetails.getState_code());
        pdf_state.setText("State : " + logeedUserDetails.getState());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());

        PdfInvoiceNo.setText(myBillDetailPojo.getInvoice_no());
        PdfDate.setText(myBillDetailPojo.getInvoice_date());
        //pdf_gstn.setText(myBillDetailPojo.getGstin_uin());
        //pdf_cin.setText(myBillDetailPojo.getInvoice_no());
        // pdf_pan.setText(logeedUserDetails.getPan());
        // pdf_state_code.setText(myBillDetailPojo.getPos());
        // pdf_state.setText(myBillDetailPojo.getState_name());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());

        pdf_cust_name.setText(myBillDetailPojo.getName_of_supplier());
        pdf_cust_add1.setText(myBillDetailPojo.getAddress());
        pdf_cust_state.setText(myBillDetailPojo.getState_name());
        pdf_cust_stateCode.setText(myBillDetailPojo.getPos());
        pdf_cust_GSTN.setText(myBillDetailPojo.getGstin_uin());
        service_desc.setText(myBillDetailPojo.getGood_service_description());
        hsn_sas_code.setText(myBillDetailPojo.getHsn_sac_code());
        total_amnt_pdf.setText(myBillDetailPojo.getInvoice_taxable_amt());
        igst_pdf.setText(myBillDetailPojo.getIgst());
        if (myBillDetailPojo.getPos().equals("07")) {
            igst_infigure.setText("IGST Amount(In figure) : " + myBillDetailPojo.getIgst());
            totalTaxAmount = myBillDetailPojo.getIgst();
        } else {
            igst_infigure.setText("CGST Amount(In figure) : " + myBillDetailPojo.getCgst());
            totalTaxAmount = myBillDetailPojo.getCgst();
        }
        int total = calcualteGrandTotal(myBillDetailPojo.getInvoice_taxable_amt(), myBillDetailPojo.getCgst());
        grand_total.setText(total + "");
        tex_amount_pdf.setText(totalTaxAmount);


        //  grand_total.setText(String.valueOf(calcualteGrandTotal(myBillDetailPojo.getInvoice_taxable_amt(), myBillDetailPojo.getCgst())));

        view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache(true);

        bitmap = Bitmap.createBitmap(view.getDrawingCache());


        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ImageView imageView = (ImageView) mDialog.findViewById(R.id.bitmap_image);
        Button downloadButton = (Button) mDialog.findViewById(R.id.download_button);
        Button shareButton = (Button) mDialog.findViewById(R.id.shareButton);

        imageView.setImageBitmap(bitmap);

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAndDownloadPdf(myBillDetailPojo, mDialog);
            }
        });
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (!hasPermissions(getActivity(), PERMISSIONS)) {

                        requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                        // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    } else if (hasPermissions(getActivity(), PERMISSIONS)) {
                        shareImage(bitmap);
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });


        mDialog.show();

    }


    public void previewBill(final MyBillDetailPojo myBillDetailPojo, final Context context) {

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(getActivity());

        // draw something on the page
        LayoutInflater inflater = (LayoutInflater)
                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = inflater.inflate(R.layout.pdf_layout, null);
        RelativeLayout relativeLayout = (RelativeLayout) content.findViewById(R.id.rotateRelative);
        relativeLayout.setRotation(90);

        TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
        ImageView pdf_logo = (ImageView) content.findViewById(R.id.pdf_logo);
        String lgog = logeedUserDetails.getCompany_logo();
        Picasso.get().load(lgog).into(pdf_logo);
        TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
        TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
        TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);


        TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
        TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
        TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
        TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
        TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
        TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
        TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);
        TextView igst_infigure = (TextView) content.findViewById(R.id.igst_infigure);
        TextView bottom_com_name = (TextView) content.findViewById(R.id.bottom_com_name);
        //............set Bank Details..............................................................
        TextView ac_name_pdf = (TextView) content.findViewById(R.id.ac_name_pdf);
        TextView bnkName_pdf = (TextView) content.findViewById(R.id.bnkName_pdf);
        TextView bank_branch_pdf = (TextView) content.findViewById(R.id.bank_branch_pdf);
        TextView AC_no_pdf = (TextView) content.findViewById(R.id.AC_no_pdf);
        TextView IFSC_pdf = (TextView) content.findViewById(R.id.IFSC_pdf);
        TextView ac_type_pdf = (TextView) content.findViewById(R.id.ac_type_pdf);

        ac_name_pdf.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());
        bnkName_pdf.setText(logeedUserDetails.getBank_name());
        bank_branch_pdf.setText(logeedUserDetails.getBank_branch());
        AC_no_pdf.setText(logeedUserDetails.getAc_no());
        IFSC_pdf.setText(logeedUserDetails.getIfsc_code());
        ac_type_pdf.setText(logeedUserDetails.getAc_type());

        TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
        TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
        TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
        TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
        TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
        TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
        TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
        TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
        TextView grand_total = (TextView) content.findViewById(R.id.grand_total);

        PdfComName.setText(logeedUserDetails.getCompany_name());
        PdfComAdd.setText(logeedUserDetails.getAddress());

        pdf_gstn.setText("GSTN : " + logeedUserDetails.getGstn());
        pdf_cin.setText("CIN  : " + logeedUserDetails.getCin());
        pdf_pan.setText("PAN : " + logeedUserDetails.getPan());
        pdf_state_code.setText("State Code : " + logeedUserDetails.getState_code());
        pdf_state.setText("State : " + logeedUserDetails.getState());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());

        PdfInvoiceNo.setText(myBillDetailPojo.getInvoice_no());
        PdfDate.setText(myBillDetailPojo.getInvoice_date());
        //pdf_gstn.setText(myBillDetailPojo.getGstin_uin());
        //pdf_cin.setText(myBillDetailPojo.getInvoice_no());
        // pdf_pan.setText(logeedUserDetails.getPan());
        // pdf_state_code.setText(myBillDetailPojo.getPos());
        // pdf_state.setText(myBillDetailPojo.getState_name());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());

        pdf_cust_name.setText(myBillDetailPojo.getName_of_supplier());
        pdf_cust_add1.setText(myBillDetailPojo.getAddress());
        pdf_cust_state.setText(myBillDetailPojo.getState_name());
        pdf_cust_stateCode.setText(myBillDetailPojo.getPos());
        pdf_cust_GSTN.setText(myBillDetailPojo.getGstin_uin());
        service_desc.setText(myBillDetailPojo.getGood_service_description());
        hsn_sas_code.setText(myBillDetailPojo.getHsn_sac_code());
        total_amnt_pdf.setText(myBillDetailPojo.getInvoice_taxable_amt());
        igst_pdf.setText(myBillDetailPojo.getIgst());
        if (myBillDetailPojo.getPos().equals("07")) {
            igst_infigure.setText("IGST Amount(In figure) : " + myBillDetailPojo.getIgst());
            totalTaxAmount = myBillDetailPojo.getIgst();
        } else {
            igst_infigure.setText("CGST Amount(In figure) : " + myBillDetailPojo.getCgst());
            totalTaxAmount = myBillDetailPojo.getCgst();
        }
        int total = calcualteGrandTotal(myBillDetailPojo.getInvoice_taxable_amt(), myBillDetailPojo.getCgst());
        grand_total.setText(total + "");
        tex_amount_pdf.setText(totalTaxAmount);


        //  grand_total.setText(String.valueOf(calcualteGrandTotal(myBillDetailPojo.getInvoice_taxable_amt(), myBillDetailPojo.getCgst())));

        view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache(true);

        bitmap = Bitmap.createBitmap(view.getDrawingCache());

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);

        final AlertDialog alertDialog = builder.create();
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.bitmap_list_view, null, false);
        ImageView imageView = (ImageView) view1.findViewById(R.id.bitmap_image);
        Button downloadButton = (Button) view1.findViewById(R.id.download_button);
        Button shareButton = (Button) view1.findViewById(R.id.shareButton);
        imageView.setImageBitmap(bitmap);

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();
        alertDialog.setView(view1);
        alertDialog.getWindow();
        WindowManager.LayoutParams params = alertDialog.getWindow().getAttributes();

        params.x = 100;
        params.y = 100;
        alertDialog.getWindow().setAttributes(params);
        alertDialog.show();

        view.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final String bitmapPath = MediaStore.Images.Media.insertImage(getActivity().getApplicationContext().getContentResolver(), bitmap, "title", null);


       /* File f = new File(Environment.getExternalStorageDirectory() + File.separator + myBillDetailPojo.getInvoice_no() + ".jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAndDownloadPdf(myBillDetailPojo, alertDialog);
            }
        });
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (!hasPermissions(getActivity(), PERMISSIONS)) {

                        requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                        // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    } else if (hasPermissions(getActivity(), PERMISSIONS)) {
                        shareImage(bitmap);
                    }


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    private void shareImage(Bitmap bitmap) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");

        String bitmapPath = MediaStore.Images.Media.insertImage(getActivity().getApplicationContext().getContentResolver(), bitmap, "title", null);
        Uri bitmapUri = Uri.parse(bitmapPath);
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        startActivity(Intent.createChooser(intent, "Share"));

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    shareImage(bitmap);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    CommonUtils.snackBar("Permission Denied By User", searchEdit, "300");
                }
                return;
            }
        }
    }

    private void createAndDownloadPdf(MyBillDetailPojo myBillDetailPojo, Dialog dialog) {


        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + myBillDetailPojo.getInvoice_no() + ".pdf";
        File file = new File(filePah);

        File file2 = new File(filePah);
        /*if (!file2.exists()) {
            file2.mkdirs();
        }*/

        try {
            if (file.exists()) {
                file.delete();
            } else {
                //file.mkdir();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                /*// draw something on the page
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.pdf_layout, null);

                TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
                TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
                TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
                TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

                TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
                TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
                TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
                TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
                TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
                TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
                TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);

                view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

                TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
                TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
                TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
                TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
                TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
                TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
                TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
                TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
                TextView grand_total = (TextView) content.findViewById(R.id.grand_total);


                PdfComName.setText("Wehyphens Pvt Ltd");
                PdfComAdd.setText("Noida Sect 64");
                PdfInvoiceNo.setText(invoiceTempData.getInvoiceNo());
                PdfDate.setText(currentDate);
                pdf_gstn.setText(invoiceTempData.getCustGstn());

                pdf_cust_name.setText(invoiceTempData.getCustName());
                pdf_cust_add1.setText(invoiceTempData.getCustAddress());
                pdf_cust_state.setText(invoiceTempData.getCustState());
                pdf_cust_stateCode.setText(invoiceTempData.getStateCode());
                pdf_cust_GSTN.setText(invoiceTempData.getCustGstn());
                service_desc.setText(invoiceTempData.getProductDesc());
                hsn_sas_code.setText(invoiceTempData.getSacOrHas());
                tex_amount_pdf.setText(amountEdit.getText().toString());
                total_amnt_pdf.setText(amountEdit.getText().toString());
                igst_pdf.setText(totalTaxAmount);
                grand_total.setText(String.valueOf(calcualteGrandTotal()));
*/

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // getActivity().finish();
                dialog.dismiss();
                Toast.makeText(getActivity(), "File Downloaded", Toast.LENGTH_SHORT).show();

            } else {
                CommonUtils.snackBar("cant create PDF on this Device", content, "300");
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    private int calcualteGrandTotal(String invoice_taxable_amt, String value) {


        String amount = invoice_taxable_amt;
        String taxAmount = totalTaxAmount;
        int GrandTotal = 0;

        if (amount != null && taxAmount != null) {
            GrandTotal = Integer.parseInt(amount) + Integer.parseInt(taxAmount);

        }

        return GrandTotal;

    }
}
