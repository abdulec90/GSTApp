package gstapp.com;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.Adapters.GalleryAdapter;
import gstapp.com.gstapp.DashBoardActivity;
import gstapp.com.gstapp.R;
import gstapp.com.login_module.LoginActivity;

public class GalleryViewActivity extends AppCompatActivity {


    @BindView(R.id.viewPager2)
    ViewPager viewPager;

    @BindView(R.id.backEdit)
    ImageView backEdit;

    @BindView(R.id.logOut)
    TextView logOut;

    @BindView(R.id.user_name)
    TextView user_name;


    @BindView(R.id.headertitle)
    TextView headertitle;

    Integer pageNumber = 0;
    String pdfFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_view);
        ButterKnife.bind(this);

        ArrayList<String> imageList = getIntent().getStringArrayListExtra("imagesList");
        String title = getIntent().getStringExtra("title");
        headertitle.setText(title);
        GalleryAdapter galleryAdapter = new GalleryAdapter(GalleryViewActivity.this, imageList, GalleryViewActivity.this);
        viewPager.setAdapter(galleryAdapter);

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(GalleryViewActivity.this, LoginActivity.class));
                CommonUtils.makeUserLogout(GalleryViewActivity.this);
                finish();
            }
        });

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(GalleryViewActivity.this);
        user_name.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());

    }

}
