package gstapp.com.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import gstapp.com.gstapp.R;

/**
 * Created by Abdul on 4/5/2018.
 */

public class MyGridAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<String> mPickedImages;

    public MyGridAdapter(Context context, ArrayList<String> mPickedImages) {

        this.context = context;
        this.mPickedImages = mPickedImages;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_recycler_item, parent, false);
        GridViewHolder gridViewHolder = new GridViewHolder(view);
        return gridViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        GridViewHolder gridViewHolder = (GridViewHolder) holder;
        Glide.with(context).load(mPickedImages.get(position)).asBitmap().into(gridViewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        if (mPickedImages != null) {
            return mPickedImages.size();
        } else {
            return 0;
        }

    }

    private class GridViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public GridViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.gridImageView);
        }
    }

}
