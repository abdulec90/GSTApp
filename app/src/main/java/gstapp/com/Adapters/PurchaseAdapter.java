package gstapp.com.Adapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import gstapp.com.fragments.UploadBills;
import gstapp.com.fragments.ViewAllPurchaseBills;

public class PurchaseAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PurchaseAdapter(FragmentManager fm, int NumOfTabs) {

        super(fm);
        this.mNumOfTabs = NumOfTabs;

    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ViewAllPurchaseBills tab1 = ViewAllPurchaseBills.newInstance();
                return tab1;
            case 1:
                UploadBills beginnersNewTab = UploadBills.newInstance();
                return beginnersNewTab;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        getItem(position).onDetach();
        //super.destroyItem(container, position, object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //super.restoreState(state, loader);
    }
}
