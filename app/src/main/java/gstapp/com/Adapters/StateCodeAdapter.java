package gstapp.com.Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ModelClasses.MyBillDetailPojo;
import gstapp.com.gstapp.EditProfileActivity;
import gstapp.com.gstapp.R;

/**
 * Created by Abdul on 4/12/2018.
 */

public class StateCodeAdapter extends BaseAdapter {


    public int selectedPosition;
    ArrayList<String> stateList;
    ArrayList<String> filterList;
    Context context;

    public StateCodeAdapter(Context context, ArrayList<String> stateList) {
        this.stateList = stateList;
        this.context = context;
        filterList = new ArrayList<>();
        this.filterList.addAll(stateList);
    }

    @Override
    public int getCount() {
        return stateList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View rowView;
        rowView = mInflater.inflate(R.layout.marital_item, null);

        final LinearLayout marridlayoutvisible = (LinearLayout) rowView.findViewById(R.id.marridlayoutvisible);

        final TextView name = (TextView) rowView.findViewById(R.id.textmaritalkstas);

        // final ImageView img = (ImageView) rowView.findViewById(R.id.iamgegreen);
        // Mapper stateModel = result.get(position);
        name.setText(stateList.get(position));

        /*if (stateModel.isSelected()) {

            img.setImageResource(R.drawable.checkbox_selected);
            //  marridlayoutvisible.startAnimation(AnimationUtils.loadAnimation(context, R.anim.citysliderimageview));

            marridlayoutvisible.setBackgroundColor(Color.parseColor("#ebeffa"));

            name.setTextColor(Color.parseColor("#3f69cc"));


        } else {

            img.setImageResource(R.drawable.checkbox_unselected);

            marridlayoutvisible.setBackgroundColor(Color.parseColor("#FFFFFF"));

            name.setTextColor(Color.parseColor("#7A7A7A"));

        }*/


        /*rowView.setOnClickListener(v -> {
            Mapper tempResult = null;
            for (int i = 0; i < result.size(); i++) {
                Mapper m = result.get(i);
                if (position == i) {
//                    m.isSeleted() ? m.setSeleted(false):m.setSeleted(true)
                    if (m.isSelected()) {
                        currentSelected.remove(m);
                        m.setSelected(false);
                    } else {
                        tempResult = m;
                        currentSelected.add(m);
                        m.setSelected(true);
                    }
                } else {
                    m.setSelected(false);
                }

            }
            if (tempResult != null) {
                stateInterface.setValue(tempResult.getName(), tempResult.getId(), type);
            } else {
                stateInterface.setValue("", "", type);
            }
            stateInterface.setValue(currentSelected, type);
            notifyDataSetChanged();
        });*/

        return rowView;
    }

  /*  public void setSelection(String id) {
        for (int i = 0; i < result.size(); i++) {
            Mapper m = result.get(i);
            if (id.equals(m.getId())) {
                m.setSelected(true);
                stateInterface.setValue(m.getName(), m.getId(), type);
                break;
            } else {
                m.setSelected(false);
            }

        }
        notifyDataSetChanged();
    }*/

/*
    public void setSelection(List<String> list) {
        if (list.size() == 0) {
            return;
        }

        for (int i = 0; i < result.size(); i++) {
            Mapper m = result.get(i);
            for (String id : list) {
                if (id.equals(m.getId())) {
                    currentSelected.add(m);
                    m.setSelected(true);
                } else {
                    m.setSelected(false);
                }
            }
        }
        stateInterface.setValue(currentSelected, type);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {

            valueFilter = new StateAdapterDialog.ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Mapper> filterList = new ArrayList<Mapper>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).getName().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        Mapper contacts = new Mapper();
                        contacts.setName(mStringFilterList.get(i).getName());
                        contacts.setId(mStringFilterList.get(i).getId());
                        filterList.add(contacts);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            result = (ArrayList<Mapper>) results.values;
            notifyDataSetChanged();
        }
    }
*/


    public void filter(String charText) {


        charText = charText.toLowerCase(Locale.getDefault());
        stateList.clear();
        if (charText.length() == 0) {
            stateList.addAll(filterList);
        } else {
            for (String wp : filterList) {
                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    stateList.add(wp);
                }
            }
        }
        notifyDataSetChanged();

    }
}
