package gstapp.com.create_invoice_module;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import UtilClasses.CommonUtils;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.MyStepperAdapter;
import gstapp.com.gstapp.R;

public class CreateInvoiceActivity extends AppCompatActivity implements
        StepperLayout.StepperListener, CreateInvoiceView {

    private StepperLayout mStepperLayout;

    @BindView(R.id.backEdit)
    ImageView backEdit;

    @BindView(R.id.user_name)
    TextView user_name;

    @BindView(R.id.logOut)
    TextView logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invoice);

        ButterKnife.bind(this);

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), CreateInvoiceActivity.this));

        LogeedUserDetails logeedUserDetails = CommonUtils.getUserDetail(CreateInvoiceActivity.this);
       // user_name.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getMName() + " " + logeedUserDetails.getLName());
        user_name.setText("userName");

        backEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onCompleted(View completeButton) {

        Toast.makeText(this, "onCompleted!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReturn() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void showProgress() {


    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onSuccess() {



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
