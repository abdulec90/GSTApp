package gstapp.com.create_invoice_module;

import UtilClasses.InvoiceTempData;

/**
 * Created by Abdul on 4/10/2018.
 */

public interface CreateInvoicePresenter {


    void createBill(InvoiceTempData invoiceTempData, String currentDate, String id);

    void onDestroy();
}
