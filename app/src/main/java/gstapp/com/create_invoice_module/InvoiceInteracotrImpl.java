package gstapp.com.create_invoice_module;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import gstapp.com.login_module.LoginInteractor;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class InvoiceInteracotrImpl implements CreateInvoiceInteractor {

    ApiInterface apiInterface = CommonUtils.InitilizeInterface();

    @Override
    public void createBill(InvoiceTempData invoiceTempData, String currentDate, String id, final CreateInvoiceInteractor.OnBillFinishedListener listener) {

        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("user_id", id);
        hashMap.put("name_of_supplier", invoiceTempData.getCustName());
        hashMap.put("gstin_uin", invoiceTempData.getCustGstn());
        hashMap.put("state_name", invoiceTempData.getCustState());
        hashMap.put("pos", invoiceTempData.getStateCode());
        hashMap.put("invoice_no", invoiceTempData.getInvoiceNo());
        hashMap.put("invoice_date", currentDate);
        hashMap.put("invoice_value", "20");
        hashMap.put("hsn_sac_code", invoiceTempData.getSacOrHas());
        hashMap.put("description", invoiceTempData.getProductDesc());
        hashMap.put("rate", invoiceTempData.getSelectedCatgeory());
        hashMap.put("invoice_taxable_amt", invoiceTempData.getTotalAmount());
        hashMap.put("qty", "1");
        hashMap.put("igst", invoiceTempData.getIGSTAmount());
        hashMap.put("igst_per", invoiceTempData.getIGSTPer());
        hashMap.put("cgst", invoiceTempData.getCGSTAmount());
        hashMap.put("cgst_per", invoiceTempData.getCGSTPer());
        hashMap.put("sgst", invoiceTempData.getSGSTAmount());
        hashMap.put("sgst_per", invoiceTempData.getSGSTAmountPer());
        hashMap.put("address", invoiceTempData.getCustAddress());

        Call<ResponseBody> bodyCall = apiInterface.CreateBill(hashMap);

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("200")) {

                        listener.onSuccess();
                        //  CommonUtils.snackBar(message, amountEdit, "200");

                    } else {
                        listener.onError();
                      //  CommonUtils.snackBar(message, amountEdit, "300");
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


                listener.onFailure();
               // CommonUtils.snackBar(t.toString(), amountEdit, "300");
                Log.e("Error", t.toString());
            }
        });

    }


}
