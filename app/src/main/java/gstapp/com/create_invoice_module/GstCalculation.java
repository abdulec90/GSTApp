package gstapp.com.create_invoice_module;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import UtilClasses.LogeedUserDetails;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.R;
import gstapp.com.retrofitClasses.APIClient;
import gstapp.com.retrofitClasses.ApiInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoViewAttacher;


public class GstCalculation extends Fragment implements Step, BlockingStep, CreateInvoiceView {


    private OnFragmentInteractionListener mListener;
    private AppCompatSpinner appCompatSpinner2;

    View content;

    @BindView(R.id.CGSTAmountEdit)
    EditText CGSTAmountEdit;

    @BindView(R.id.SGSTAmountEdit)
    EditText SGSTAmountEdit;

    @BindView(R.id.IGSTAmountEdit)
    EditText IGSTAmountEdit;

    @BindView(R.id.CGSTEdit)
    EditText CGSTEdit;

    @BindView(R.id.SGSTEdit)
    EditText SGSTEdit;

    @BindView(R.id.IGSTEdit)
    EditText IGSTEdit;

    @BindView(R.id.amountEdit)
    EditText amountEdit;

    @BindView(R.id.submit_button)
    Button submit_button;

    @BindView(R.id.preview_button)
    Button preview_button;

    private String selectedCatgeory;
    private int enteredAmount;
    private String totalTaxAmount;
    String stateCode;
    private ProgressDialog progressDailog;
    private String currentDate;
    private ApiInterface apiInterface;

    InvoiceTempData invoiceTempData;
    private LinearLayout view;

    LogeedUserDetails logeedUserDetails;
    private CreateInvoicePresenter createInvoicePresenter;

    public GstCalculation() {
        // Required empty public constructor
    }


    public static GstCalculation newInstance(String param1, String param2) {
        GstCalculation fragment = new GstCalculation();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gst_calculation, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        logeedUserDetails = CommonUtils.getUserDetail(getActivity());
        appCompatSpinner2 = (AppCompatSpinner) getView().findViewById(R.id.spinner2);

        invoiceTempData = InvoiceTempData.getInstance();
        stateCode = invoiceTempData.getStateCode();

        createInvoicePresenter = new InvoicePresenterImpl(this, new InvoiceInteracotrImpl());

        apiInterface = APIClient.getClient().create(ApiInterface.class);

        progressDailog = new ProgressDialog(getActivity());
        progressDailog.setMessage("Please wait...");

        LinkedList<String> taxCat = new LinkedList<>();
        taxCat.add("28%");
        taxCat.add("18%");
        taxCat.add("5%");

        // Toast.makeText(getActivity(), "Invoice No : " + invoiceTempData.getInvoiceNo(), Toast.LENGTH_SHORT).show();

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, taxCat);
        appCompatSpinner2.setAdapter(arrayAdapter2);

        appCompatSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCatgeory = appCompatSpinner2.getSelectedItem().toString().replace("%", "");

                float selectedValue = Float.parseFloat(selectedCatgeory);


                if (stateCode.equals("07")) {
                    CGSTEdit.setText(selectedValue / 2 + " %");
                    SGSTEdit.setText(selectedValue / 2 + " %");

                    IGSTEdit.setText("");
                    IGSTAmountEdit.setText("");
                } else {
                    CGSTEdit.setText("");
                    SGSTEdit.setText("");

                    IGSTEdit.setText(selectedValue + " %");
                }

                calculateAmount(selectedValue);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        preview_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                invoiceTempData.setSelectedCatgeory(selectedCatgeory);
                invoiceTempData.setTotalAmount(amountEdit.getText().toString());
                invoiceTempData.setIGSTAmount(IGSTAmountEdit.getText().toString());
                invoiceTempData.setIGSTPer(IGSTEdit.getText().toString().replace("%", ""));
                invoiceTempData.setCGSTAmount(CGSTAmountEdit.getText().toString());
                invoiceTempData.setCGSTPer(CGSTEdit.getText().toString().replace("%", ""));
                invoiceTempData.setSGSTAmount(SGSTAmountEdit.getText().toString());
                invoiceTempData.setSGSTAmountPer(SGSTEdit.getText().toString().replace("%", ""));

                showBitmap();

            }
        });

    }

    private void showBitmap() {

        // draw something on the page
        LayoutInflater inflater = (LayoutInflater)
                getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        content = inflater.inflate(R.layout.pdf_layout, null);

        TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
        ImageView pdf_logo = (ImageView) content.findViewById(R.id.pdf_logo);
        TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
        TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
        TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

        TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
        TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
        TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
        TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
        TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
        TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
        TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);
        TextView igst_infigure = (TextView) content.findViewById(R.id.igst_infigure);
        TextView bottom_com_name = (TextView) content.findViewById(R.id.bottom_com_name);
        //............set Bank Details..............................................................
        TextView ac_name_pdf = (TextView) content.findViewById(R.id.ac_name_pdf);
        TextView bnkName_pdf = (TextView) content.findViewById(R.id.bnkName_pdf);
        TextView bank_branch_pdf = (TextView) content.findViewById(R.id.bank_branch_pdf);
        TextView AC_no_pdf = (TextView) content.findViewById(R.id.AC_no_pdf);
        TextView IFSC_pdf = (TextView) content.findViewById(R.id.IFSC_pdf);
        TextView ac_type_pdf = (TextView) content.findViewById(R.id.ac_type_pdf);

        ac_name_pdf.setText(logeedUserDetails.getfName() + " " + logeedUserDetails.getLName());
        bnkName_pdf.setText(logeedUserDetails.getBank_name());
        bank_branch_pdf.setText(logeedUserDetails.getBank_branch());
        AC_no_pdf.setText(logeedUserDetails.getAc_no());
        IFSC_pdf.setText(logeedUserDetails.getIfsc_code());
        ac_type_pdf.setText(logeedUserDetails.getAc_type());


        TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
        TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
        TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
        TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
        TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
        TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
        TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
        TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
        TextView grand_total = (TextView) content.findViewById(R.id.grand_total);


        PdfComName.setText(logeedUserDetails.getCompany_name());
        PdfComAdd.setText(logeedUserDetails.getAddress());
        PdfInvoiceNo.setText("Invoice No : \n" + invoiceTempData.getInvoiceNo());
        PdfDate.setText(currentDate);
        pdf_gstn.setText("GSTN : " + logeedUserDetails.getGstn());
        pdf_cin.setText("CIN  : " + logeedUserDetails.getCin());
        pdf_pan.setText("PAN : " + logeedUserDetails.getPan());
        pdf_state_code.setText("State Code : " + logeedUserDetails.getState_code());
        pdf_state.setText("State : " + logeedUserDetails.getState());
        bottom_com_name.setText(logeedUserDetails.getCompany_name());

        Picasso.get().load(logeedUserDetails.getCompany_logo()).into(pdf_logo);

        pdf_cust_name.setText(invoiceTempData.getCustName());
        pdf_cust_add1.setText(invoiceTempData.getCustAddress());
        pdf_cust_state.setText(invoiceTempData.getCustState());
        pdf_cust_stateCode.setText(invoiceTempData.getStateCode());
        pdf_cust_GSTN.setText(invoiceTempData.getCustGstn());
        service_desc.setText(invoiceTempData.getProductDesc());
        hsn_sas_code.setText(invoiceTempData.getSacOrHas());
        tex_amount_pdf.setText(amountEdit.getText().toString());
        total_amnt_pdf.setText(amountEdit.getText().toString());
        igst_pdf.setText(totalTaxAmount);
        if (stateCode.equals("07")) {
            igst_infigure.setText("IGST Amount(In figure) : " + totalTaxAmount);
        } else {
            igst_infigure.setText("CGST Amount(In figure) : " + totalTaxAmount);
        }

        grand_total.setText(String.valueOf(calcualteGrandTotal()));

        view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache(true);

        Bitmap b = Bitmap.createBitmap(view.getDrawingCache());

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view1 = layoutInflater.inflate(R.layout.bitmap_view, null, false);
        ImageView imageView = (ImageView) view1.findViewById(R.id.bitmap_image);
        Button submitForm = (Button) view1.findViewById(R.id.submitForm);
        imageView.setImageBitmap(b);

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView);
        pAttacher.update();

        builder.setView(view1);

        submitForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = builder.create();
                alertDialog.dismiss();

                getCurrentData();


            }
        });

        builder.show();

        view.setDrawingCacheEnabled(false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "v2i.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (Exception e) {
        }

    }

    private void getCurrentData() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(date);
        currentDate = formattedDate;

        SubmitDetails();
    }

    private void SubmitDetails() {

        String id = CommonUtils.getUserId(getActivity());

        createInvoicePresenter.createBill(invoiceTempData, currentDate, id);

       /* HashMap<String, String> hashMap = new HashMap();
        hashMap.put("user_id", id);
        hashMap.put("name_of_supplier", invoiceTempData.getCustName());
        hashMap.put("gstin_uin", invoiceTempData.getCustGstn());
        hashMap.put("state_name", invoiceTempData.getCustState());
        hashMap.put("pos", invoiceTempData.getStateCode());
        hashMap.put("invoice_no", invoiceTempData.getInvoiceNo());
        hashMap.put("invoice_date", currentDate);
        hashMap.put("invoice_value", "20");
        hashMap.put("hsn_sac_code", invoiceTempData.getSacOrHas());
        hashMap.put("description", invoiceTempData.getProductDesc());
        hashMap.put("rate", selectedCatgeory);
        hashMap.put("invoice_taxable_amt", amountEdit.getText().toString());
        hashMap.put("qty", "1");
        hashMap.put("igst", IGSTAmountEdit.getText().toString());
        hashMap.put("igst_per", IGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("cgst", CGSTAmountEdit.getText().toString());
        hashMap.put("cgst_per", CGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("sgst", SGSTAmountEdit.getText().toString());
        hashMap.put("sgst_per", SGSTEdit.getText().toString().replace("%", ""));
        hashMap.put("address", invoiceTempData.getCustAddress());*/

        //  Call<ResponseBody> bodyCall = apiInterface.CreateBill(hashMap);

      /*  bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDailog.dismiss();
                try {
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("200")) {
                        saveLocal();
                        CreatePDf();
                        // ShowThankYouScreen();
                        CommonUtils.snackBar(message, amountEdit, "200");

                    } else {
                        CommonUtils.snackBar(message, amountEdit, "300");
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDailog.dismiss();
                CommonUtils.snackBar(t.toString(), amountEdit, "300");
                Log.e("Error", t.toString());
            }
        });*/
    }

    private void CreatePDf() {

        String externalStorageDirectory = "" + Environment.getExternalStorageDirectory().getAbsolutePath();
        String filePah = externalStorageDirectory + "/GST/" + invoiceTempData.getInvoiceNo() + ".pdf";
        File file = new File(filePah);

        try {
            if (file.exists()) {
                file.delete();
            }
            file.getParentFile().mkdirs();

            file.createNewFile();

            PdfDocument.PageInfo pageInfo = null;
            FileOutputStream outputStream;
            PdfDocument pdfDocument;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                pageInfo = new PdfDocument.PageInfo.Builder(2250, 1400, 1).create();

                // start a page
                pdfDocument = new PdfDocument();
                PdfDocument.Page page = pdfDocument.startPage(pageInfo);

                /*// draw something on the page
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.pdf_layout, null);

                TextView PdfComName = (TextView) content.findViewById(R.id.pdf_come_name);
                TextView PdfComAdd = (TextView) content.findViewById(R.id.pdf_com_address);
                TextView PdfInvoiceNo = (TextView) content.findViewById(R.id.pdf_invoice);
                TextView PdfDate = (TextView) content.findViewById(R.id.pdf_date);

                TextView pdf_gstn = (TextView) content.findViewById(R.id.pdf_gstn);
                TextView pdf_cin = (TextView) content.findViewById(R.id.pdf_cin);
                TextView pdf_pan = (TextView) content.findViewById(R.id.pdf_pan);
                TextView pdf_state_code = (TextView) content.findViewById(R.id.pdf_state_code);
                TextView pdf_state = (TextView) content.findViewById(R.id.pdf_state);
                TextView pdf_cust_name = (TextView) content.findViewById(R.id.pdf_cust_name);
                TextView igst_pdf = (TextView) content.findViewById(R.id.igst_pdf);

                view = (LinearLayout) content.findViewById(R.id.bitmap_layout);

                TextView pdf_cust_add1 = (TextView) content.findViewById(R.id.pdf_cust_add1);
                TextView pdf_cust_state = (TextView) content.findViewById(R.id.pdf_cust_state);
                TextView pdf_cust_stateCode = (TextView) content.findViewById(R.id.pdf_cust_stateCode);
                TextView pdf_cust_GSTN = (TextView) content.findViewById(R.id.pdf_cust_GSTN);
                TextView service_desc = (TextView) content.findViewById(R.id.service_desc);
                TextView hsn_sas_code = (TextView) content.findViewById(R.id.hsn_sas_code);
                TextView tex_amount_pdf = (TextView) content.findViewById(R.id.tex_amount_pdf);
                TextView total_amnt_pdf = (TextView) content.findViewById(R.id.total_amnt_pdf);
                TextView grand_total = (TextView) content.findViewById(R.id.grand_total);


                PdfComName.setText("Wehyphens Pvt Ltd");
                PdfComAdd.setText("Noida Sect 64");
                PdfInvoiceNo.setText(invoiceTempData.getInvoiceNo());
                PdfDate.setText(currentDate);
                pdf_gstn.setText(invoiceTempData.getCustGstn());

                pdf_cust_name.setText(invoiceTempData.getCustName());
                pdf_cust_add1.setText(invoiceTempData.getCustAddress());
                pdf_cust_state.setText(invoiceTempData.getCustState());
                pdf_cust_stateCode.setText(invoiceTempData.getStateCode());
                pdf_cust_GSTN.setText(invoiceTempData.getCustGstn());
                service_desc.setText(invoiceTempData.getProductDesc());
                hsn_sas_code.setText(invoiceTempData.getSacOrHas());
                tex_amount_pdf.setText(amountEdit.getText().toString());
                total_amnt_pdf.setText(amountEdit.getText().toString());
                igst_pdf.setText(totalTaxAmount);
                grand_total.setText(String.valueOf(calcualteGrandTotal()));
*/

                int measureWidth = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getWidth(), View.MeasureSpec.EXACTLY);
                int measuredHeight = View.MeasureSpec.makeMeasureSpec(page.getCanvas().getHeight() + 200, View.MeasureSpec.EXACTLY);

                content.measure(measureWidth, measuredHeight);
                content.layout(0, 0, page.getCanvas().getWidth(), page.getCanvas().getHeight());

                content.draw(page.getCanvas());

                // finish the page
                pdfDocument.finishPage(page);
                // add more pages
                // write the document content
                outputStream = new FileOutputStream(file);
                try {
                    pdfDocument.writeTo(outputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                CommonUtils.snackBar("Bill Generated", amountEdit, "200");
                // getActivity().finish();
            } else {
                CommonUtils.snackBar("cant create PDF on this Device", amountEdit, "300");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private int calcualteGrandTotal() {

        String amount = amountEdit.getText().toString();
        String taxAmount = totalTaxAmount;
        int GrandTotal = 0;

        if (amount != null && taxAmount != null) {
            GrandTotal = Integer.parseInt(amount) + Integer.parseInt(taxAmount);

        }

        return GrandTotal;
    }

    private void saveLocal() {

        String bilNo = CommonUtils.getBillNo(getActivity());
        String numOnly = bilNo.replaceAll("[\\D]", "");
        long num = Long.parseLong(numOnly);
        String newstr = bilNo.replaceAll("[^A-Za-z]+", "");

        long newNum = num + 1;

        String requiredString = newstr + newNum;

        saveInvoiceLocal(requiredString);
    }

    private void saveInvoiceLocal(String requiredString) {

        CommonUtils.saveBillNo(getActivity(), requiredString);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    private void calculateAmount(float selectedValue) {

        int percentAmount;
        String hh = amountEdit.getText().toString();
        if (hh != null && !hh.equals("")) {

            enteredAmount = Integer.parseInt(hh);

            if (stateCode.equals("07")) {
                percentAmount = (int) (enteredAmount * (selectedValue / 2) / 100);
                totalTaxAmount = String.valueOf(percentAmount * 2);
                CGSTAmountEdit.setText(percentAmount + "");
                SGSTAmountEdit.setText(percentAmount + "");

                IGSTAmountEdit.setText("");

            } else {
                percentAmount = (int) (enteredAmount * (selectedValue) / 100);
                totalTaxAmount = String.valueOf(percentAmount);

                CGSTAmountEdit.setText("");
                SGSTAmountEdit.setText("");

                IGSTAmountEdit.setText(percentAmount + "");
            }
        }
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        getActivity().onBackPressed();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

    }

    @Override
    public void showProgress() {

        progressDailog.show();
    }

    @Override
    public void hideProgress() {

        progressDailog.dismiss();
    }

    @Override
    public void onSuccess() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                saveLocal();
            }
        });

        CreatePDf();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        createInvoicePresenter.onDestroy();

    }
}
