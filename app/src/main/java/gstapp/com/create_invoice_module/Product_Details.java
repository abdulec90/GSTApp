package gstapp.com.create_invoice_module;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import UtilClasses.CommonUtils;
import UtilClasses.InvoiceTempData;
import butterknife.BindView;
import butterknife.ButterKnife;
import gstapp.com.gstapp.R;


public class Product_Details extends Fragment implements Step {


    AppCompatSpinner appCompatSpinner2;
    @BindView(R.id.product_desc)
    EditText product_desc;

    @BindView(R.id.sac_HSN)
    EditText sac_HSN;

    @BindView(R.id.secondInvoiceNo)
    TextView secondInvoiceNo;

    InvoiceTempData invoiceTempData;


    public Product_Details() {
        // Required empty public constructor
    }


    public static Product_Details newInstance() {
        Product_Details fragment = new Product_Details();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product__details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        invoiceTempData = InvoiceTempData.getInstance();
        secondInvoiceNo.setText(invoiceTempData.getInvoiceNo());

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {

        if (product_desc.length() == 0) {
            CommonUtils.snackBar("Please enter Product Description", product_desc,"300");
            return new VerificationError("Please enter Product Description");
        } else if (sac_HSN.length() == 0) {
            CommonUtils.snackBar("Please enter HSN/SAC", sac_HSN,"300");
            return new VerificationError("Please enter Product Description");
        } else {

            invoiceTempData.setProductDesc(product_desc.getText().toString());
            invoiceTempData.setSacOrHas(sac_HSN.getText().toString());
            return null;
        }
    }

    @Override
    public void onSelected() {
        Log.v("OnSelected", this.toString());
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
