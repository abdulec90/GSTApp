package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyBillDetailPojo {


    @Expose
    @SerializedName("company_id")
    private String company_id;

    @Expose
    @SerializedName("gstin_uin")
    private String gstin_uin;

    @Expose
    @SerializedName("sgst_per")
    private String sgst_per;

    @Expose
    @SerializedName("invoice_value")
    private String invoice_value;

    @Expose
    @SerializedName("good_service_description")
    private String good_service_description;

    @Expose
    @SerializedName("qty")
    private String qty;

    @Expose
    @SerializedName("invoice_date")
    private String invoice_date;

    @Expose
    @SerializedName("sgst")
    private String sgst;

    @Expose
    @SerializedName("state_name")
    private String state_name;

    @Expose
    @SerializedName("hsn_sac_code")
    private String hsn_sac_code;

    @Expose
    @SerializedName("pos")
    private String pos;

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("rate")
    private String rate;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("cgst_per")
    private String cgst_per;

    @Expose
    @SerializedName("igst")
    private String igst;

    @Expose
    @SerializedName("name_of_supplier")
    private String name_of_supplier;

    @Expose
    @SerializedName("cgst")
    private String cgst;

    @Expose
    @SerializedName("igst_per")
    private String igst_per;

    @Expose
    @SerializedName("invoice_no")
    private String invoice_no;

    @Expose
    @SerializedName("invoice_taxable_amt")
    private String invoice_taxable_amt;

    public String getCompany_id ()
    {
        return company_id;
    }

    public void setCompany_id (String company_id)
    {
        this.company_id = company_id;
    }

    public String getGstin_uin ()
    {
        return gstin_uin;
    }

    public void setGstin_uin (String gstin_uin)
    {
        this.gstin_uin = gstin_uin;
    }

    public String getSgst_per ()
    {
        return sgst_per;
    }

    public void setSgst_per (String sgst_per)
    {
        this.sgst_per = sgst_per;
    }

    public String getInvoice_value ()
    {
        return invoice_value;
    }

    public void setInvoice_value (String invoice_value)
    {
        this.invoice_value = invoice_value;
    }

    public String getGood_service_description ()
    {
        return good_service_description;
    }

    public void setGood_service_description (String good_service_description)
    {
        this.good_service_description = good_service_description;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    public String getInvoice_date ()
    {
        return invoice_date;
    }

    public void setInvoice_date (String invoice_date)
    {
        this.invoice_date = invoice_date;
    }

    public String getSgst ()
    {
        return sgst;
    }

    public void setSgst (String sgst)
    {
        this.sgst = sgst;
    }

    public String getState_name ()
    {
        return state_name;
    }

    public void setState_name (String state_name)
    {
        this.state_name = state_name;
    }

    public String getHsn_sac_code ()
    {
        return hsn_sac_code;
    }

    public void setHsn_sac_code (String hsn_sac_code)
    {
        this.hsn_sac_code = hsn_sac_code;
    }

    public String getPos ()
    {
        return pos;
    }

    public void setPos (String pos)
    {
        this.pos = pos;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getCgst_per ()
    {
        return cgst_per;
    }

    public void setCgst_per (String cgst_per)
    {
        this.cgst_per = cgst_per;
    }

    public String getIgst ()
    {
        return igst;
    }

    public void setIgst (String igst)
    {
        this.igst = igst;
    }

    public String getName_of_supplier ()
    {
        return name_of_supplier;
    }

    public void setName_of_supplier (String name_of_supplier)
    {
        this.name_of_supplier = name_of_supplier;
    }

    public String getCgst ()
    {
        return cgst;
    }

    public void setCgst (String cgst)
    {
        this.cgst = cgst;
    }

    public String getIgst_per ()
    {
        return igst_per;
    }

    public void setIgst_per (String igst_per)
    {
        this.igst_per = igst_per;
    }

    public String getInvoice_no ()
    {
        return invoice_no;
    }

    public void setInvoice_no (String invoice_no)
    {
        this.invoice_no = invoice_no;
    }

    public String getInvoice_taxable_amt ()
    {
        return invoice_taxable_amt;
    }

    public void setInvoice_taxable_amt (String invoice_taxable_amt)
    {
        this.invoice_taxable_amt = invoice_taxable_amt;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [company_id = "+company_id+", gstin_uin = "+gstin_uin+", sgst_per = "+sgst_per+", invoice_value = "+invoice_value+", good_service_description = "+good_service_description+", qty = "+qty+", invoice_date = "+invoice_date+", sgst = "+sgst+", state_name = "+state_name+", hsn_sac_code = "+hsn_sac_code+", pos = "+pos+", id = "+id+", rate = "+rate+", address = "+address+", cgst_per = "+cgst_per+", igst = "+igst+", name_of_supplier = "+name_of_supplier+", cgst = "+cgst+", igst_per = "+igst_per+", invoice_no = "+invoice_no+", invoice_taxable_amt = "+invoice_taxable_amt+"]";
    }
}
