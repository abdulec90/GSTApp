package ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyBillMainPojo {

    @Expose
    @SerializedName("response")
    private String response;

    public String getBasicRate() {
        return basicRate;
    }

    public void setBasicRate(String basicRate) {
        this.basicRate = basicRate;
    }

    public String getTotalInvoice() {
        return totalInvoice;
    }

    public void setTotalInvoice(String totalInvoice) {
        this.totalInvoice = totalInvoice;
    }

    @Expose
    @SerializedName("basicRate")
    private String basicRate;

    @Expose
    @SerializedName("totalInvoice")
    private String totalInvoice;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("status")
    private String status;

    @Expose
    @SerializedName("bills")
    private List<MyBillDetailPojo> bills;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MyBillDetailPojo> getBills() {
        return bills;
    }

    public void setBills(List<MyBillDetailPojo> bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", message = " + message + ", status = " + status + ", bills = " + bills + "]";
    }
}
