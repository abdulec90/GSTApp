package ModelClasses;

import java.util.List;

/**
 * Created by Abdul on 4/6/2018.
 */

public class AllBillMainPojo {


    private List<AllBillsListPojo> response;

    private String message;

    private String status;

    public List<AllBillsListPojo> getResponse() {
        return response;
    }

    public void setResponse(List<AllBillsListPojo> response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [response = " + response + ", message = " + message + ", status = " + status + "]";
    }
}
