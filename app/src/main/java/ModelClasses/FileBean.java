package ModelClasses;

/**
 * Created by Abdul on 4/2/2018.
 */

public class FileBean {

    String fileName;
    String filePath;
    boolean isClicked=false;

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public FileBean(String fileName, String filePath) {
        this.fileName = fileName;
        this.filePath = filePath;
    }
}
