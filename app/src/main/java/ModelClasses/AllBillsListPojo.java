package ModelClasses;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllBillsListPojo {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("company_id")
    private String company_id;

    @Expose
    @SerializedName("create_at")
    private String create_at;

    @Expose
    @SerializedName("category")
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Expose
    @SerializedName("bills")
    private String bills;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getBills() {
        return bills;
    }

    public void setBills(String bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", company_id = " + company_id + ", create_at = " + create_at + ",category = " + category + ", bills = " + bills + "]";
    }
}
