package UtilClasses;


public class InvoiceTempData {

    public static InvoiceTempData invoiceTempData;

    public static InvoiceTempData getInstance() {

        if (invoiceTempData == null) {
            invoiceTempData = new InvoiceTempData();
        }

        return invoiceTempData;
    }


    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCustGstn() {
        return custGstn;
    }

    public void setCustGstn(String custGstn) {
        this.custGstn = custGstn;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getSacOrHas() {
        return sacOrHas;
    }

    public void setSacOrHas(String sacOrHas) {
        this.sacOrHas = sacOrHas;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    private String custName;
    private String custAddress;
    private String custState;
    private String stateCode;
    private String custGstn;
    private String productDesc;
    private String sacOrHas;
    private String amount;
    private String taxAmount;
    private String invoiceNo;
    private String selectedCatgeory;
    private String totalAmount;
    private String IGSTAmount;
    private String IGSTPer;
    private String CGSTAmount;
    private String CGSTPer;
    private String SGSTAmount;
    private String SGSTAmountPer;

    public String getSelectedCatgeory() {
        return selectedCatgeory;
    }

    public void setSelectedCatgeory(String selectedCatgeory) {
        this.selectedCatgeory = selectedCatgeory;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getIGSTAmount() {
        return IGSTAmount;
    }

    public void setIGSTAmount(String IGSTAmount) {
        this.IGSTAmount = IGSTAmount;
    }

    public String getIGSTPer() {
        return IGSTPer;
    }

    public void setIGSTPer(String IGSTPer) {
        this.IGSTPer = IGSTPer;
    }

    public String getCGSTAmount() {
        return CGSTAmount;
    }

    public void setCGSTAmount(String CGSTAmount) {
        this.CGSTAmount = CGSTAmount;
    }

    public String getCGSTPer() {
        return CGSTPer;
    }

    public void setCGSTPer(String CGSTPer) {
        this.CGSTPer = CGSTPer;
    }

    public String getSGSTAmount() {
        return SGSTAmount;
    }

    public void setSGSTAmount(String SGSTAmount) {
        this.SGSTAmount = SGSTAmount;
    }

    public String getSGSTAmountPer() {
        return SGSTAmountPer;
    }

    public void setSGSTAmountPer(String SGSTAmountPer) {
        this.SGSTAmountPer = SGSTAmountPer;
    }

    public static InvoiceTempData getInvoiceTempData() {
        return invoiceTempData;
    }

    public static void setInvoiceTempData(InvoiceTempData invoiceTempData) {
        InvoiceTempData.invoiceTempData = invoiceTempData;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
}
