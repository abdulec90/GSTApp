package UtilClasses;

import android.net.Uri;

public interface ImageInterface {

    void onImageReceived(Uri uri);
}
